#!/bin/bash
#################################################################
#  @package Hcore
#  @subpackage vHost
#  @author Alessandro Stucchi <wolfgan@gmail.com>
#################################################################

version="2.0"
logo()
{
#http://patorjk.com/software/taag/#p=display&f=Slant&t=Hcore%20Vhost
#font: slant
    clear
    cat << "EOF"

    __  __                        _    ____               __
   / / / /________  ________     | |  / / /_  ____  _____/ /_
  / /_/ / ___/ __ \/ ___/ _ \    | | / / __ \/ __ \/ ___/ __/
 / __  / /__/ /_/ / /  /  __/    | |/ / / / / /_/ (__  ) /_
/_/ /_/\___/\____/_/   \___/     |___/_/ /_/\____/____/\__/
                                                        v2.0

EOF
}

#Administrator Contact
  adminC=itis.team@healthwareinternational.com

#Default Configurations
scriptPath=$PWD
php=$(php -r "echo PHP_MAJOR_VERSION.PHP_MINOR_VERSION;")
documentRoot='/data/www'
databaseRoot='/data/database'
logRoot='/data/log/www'

myUser=$(whoami)
vHost=$1

shopt -s expand_aliases
source /etc/bashrc

logo

if [[ -z "$vHost" ]]
  then
    read -p "Domain Name (es. test.hcore.app): " vHost
fi

if [[ -n "$vHost" ]]; then
    echo =================================================================
    echo - Create VirtualHost
    echo -----------------------------------------------------------------
    currPath=$documentRoot/$vHost

    echo - MakeDir: $currPath/public
    mkdir -p $currPath/public
    echo - MakeDir: $currPath/logs
    mkdir -p $currPath/logs
    echo - MakeDir: $currPath/libs
    mkdir -p $currPath/libs
    echo - MakeDir: $currPath/profiler
    mkdir -p $currPath/profiler
    echo - MakeDir: $currPath/tests
    mkdir -p $currPath/tests
    echo - Done.

    echo =================================================================
    echo - Apache VirtualHost
    echo -----------------------------------------------------------------
    if [[ -n "$php" ]]; then
        echo - Handler PHP: php$php-fcgi
        handlerDefault="AddHandler php$php-fcgi .php"
        openBaseDir="php_admin_value open_basedir \"$currPath/public:/tmp\""
    fi

    apacheRoot="/etc/httpd"
    # Making Wrapper Files in cgi-bin
    if [[ ! -d "$apacheRoot" ]]; then
        apacheRoot="/etc/apache2"
    fi

    # Making Wrapper Files in cgi-bin
    echo - Add: $apacheRoot/sites-enabled/$vHost.conf
    cat > $apacheRoot/sites-enabled/$vHost.conf << EOF
<VirtualHost *:80>
  AddDefaultCharset UTF-8
  SetEnvIf Authorization "(.*)" HTTP_AUTHORIZATION=\$1

	ServerName $vHost
	ServerAlias $vHost
	DocumentRoot $currPath/public
	$handlerDefault
	ErrorLog $logRoot/$vHost-error.log
	CustomLog $logRoot/$vHost-requests.log common

  #Header always set Content-Type "application/json"
  #ErrorDocument 403 "{\"status\" : 403, \"error\" : \"Service Available only with ssl protocol: https://$vHost\"}"

  #RewriteEngine On
  #RewriteRule ^ - [L,R=403]

  <Directory "$currPath/public">
    #$openBaseDir
    Options -Indexes +FollowSymLinks +MultiViews
    DirectoryIndex index.html index.php
    AllowOverride All
    Require all granted
    Order allow,deny
    allow from all
  </Directory>
</VirtualHost>
EOF

    # Making Wrapper Files in cgi-bin
    echo - Add: $apacheRoot/sites-enabled/$vHost-ssl.conf
    cat > $apacheRoot/sites-enabled/$vHost-ssl.conf << EOF
<VirtualHost *:443>
  AddDefaultCharset UTF-8
  SetEnvIf Authorization "(.*)" HTTP_AUTHORIZATION=\$1

	ServerName $vHost
	ServerAlias $vHost
	DocumentRoot $currPath/public
	$handlerDefault
	ErrorLog $logRoot/$vHost-ssl-error.log
	CustomLog $logRoot/$vHost-ssl-requests.log common

	<Directory "$currPath/public">
	  #$openBaseDir
	  Options -Indexes +FollowSymLinks +MultiViews
    DirectoryIndex index.html index.php
		AllowOverride All
		Require all granted
		Order allow,deny
		allow from all
	</Directory>
</VirtualHost>
EOF
    echo - Done.
    echo =================================================================
    echo - Apache Restart
    echo -----------------------------------------------------------------
    service httpd stop
    service httpd start
    echo - Done.
    echo =================================================================
    echo - Apache Certificate SSL
    echo -----------------------------------------------------------------
    certbot --apache --non-interactive -d $vHost -m $adminC --agree-tos
    echo - Done.

    echo =================================================================
    echo - Apache SymLink logs
    echo -----------------------------------------------------------------
    if [[ ! -e $currPath/logs/error.log ]]; then
        ln -s $logRoot/$vHost-error.log $currPath/logs/error.log
    fi
    if [[ ! -e $currPath/logs/requests.log ]]; then
        ln -s $logRoot/$vHost-requests.log $currPath/logs/requests.log
    fi
    if [[ ! -e $currPath/logs/ssl-error.log ]]; then
        ln -s $logRoot/$vHost-ssl-error.log $currPath/logs/ssl-error.log
    fi
    if [[ ! -e $currPath/logs/ssl-requests.log ]]; then
        ln -s $logRoot/$vHost-ssl-requests.log $currPath/logs/ssl-requests.log
    fi
    echo - Done

#    export COMPOSER_ALLOW_SUPERUSER=1

#    echo =================================================================
#    echo - Hcore CLI Install
#    echo -----------------------------------------------------------------
#    cd $currPath/public
#    composer global require hcore/cli
#    if ! grep -Fxq 'export PATH="$PATH:$HOME/.config/composer/vendor/bin"' ~/.bashrc; then
#      echo 'export PATH="$PATH:$HOME/.config/composer/vendor/bin"' >> ~/.bashrc
#    fi
#    source ~/.bashrc
#    echo - Done

#    echo =================================================================
#    echo - Hcore vHost Install
#    echo -----------------------------------------------------------------
#    hcore create $vHost

    echo - Done.

    echo =================================================================
    echo - Hcore Set Folder Permissions
    echo -----------------------------------------------------------------
    # myUser=false

    source $scriptPath/hcore-vhost-permission.sh $vHost
fi

echo =================================================================
echo - Installation Finished
echo =================================================================
