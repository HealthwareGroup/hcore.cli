<?php
error_reporting(E_ALL);
ini_set('display_errors', "On");

use hcore\Hcore;
use hcore\libs\storage\migration\Migration;

$cmd = $argv[1] ?: "preview";
if (empty($domain = $argv[2])) {
    echo "domain is required";
}
/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| our application. We just need to utilize it! We'll simply require it
| into the script here so that we don't have to worry about manual
| loading any of our classes later on. It feels great to relax.
|
*/
require_once("/data/www/" . $domain . "/public/vendor/autoload.php");

/*
|--------------------------------------------------------------------------
| Init
|--------------------------------------------------------------------------
| configuration Server Environment
|
*/
require_once("/data/www/" . $domain . "/public/config.php");
require_once("Colors.php");

try {
    $hcore = new Hcore(Config::class);
    $result = Migration::orm2db($cmd);
    foreach ($result as $type => $tables) {
        foreach ($tables ?: [] as $table => $queries) {
            foreach ($queries as $query_type => $query) {
                $color = $type == "safe" ? Colors::green : Colors::yellow;
                echo  "$color*** ". strtoupper($type) . " *** " . Colors::reset . strtoupper($query_type) . " [". Colors::magenta . $table . Colors::reset . "] " . $query ."\n";
            }
        }
        echo $tables ? str_pad("", 100, "-")."\n\n" : "";
    }
} catch (Exception $e) {
    echo $e->getMessage();
}
exit;
