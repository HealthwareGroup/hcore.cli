#!/bin/bash
#################################################################
#  @package Hcore
#  @subpackage User
#  @author Alessandro Stucchi <wolfgan@gmail.com>
#################################################################

version="1.0"
logo()
{
#http://patorjk.com/software/taag/#p=display&f=Slant&t=Hcore%20User
#font: slant
    clear
    cat << "EOF"

    __  __                        __  __
   / / / /________  ________     / / / /_______  _____
  / /_/ / ___/ __ \/ ___/ _ \   / / / / ___/ _ \/ ___/
 / __  / /__/ /_/ / /  /  __/  / /_/ (__  )  __/ /
/_/ /_/\___/\____/_/   \___/   \____/____/\___/_/
                                                    v1.0

EOF
}

#Default Configurations
documentRoot='/data/www'
databaseRoot='/data/database'
logRoot='/data/log/www'

myUser=$(whoami)
user=$1
pw=$2
vHost=$3
primary=developers

logo

if [[ -z "$user" ]]
  then
    read -p "UserName (es. jdoe): " user
fi

if [[ -z "$pw" ]]
  then
    read -p "Password (es. MySecret1!): " pw
fi

if [[ -z "$vHost" ]]
  then
    read -p "Domain Name (es. my.hcore.app): " vHost
fi

echo =================================================================
echo - Hcore Set Permissions: $user
echo -----------------------------------------------------------------
if [[ "$myUser" == "root" ]]; then
  groupadd $primary
  groupadd $vHost 					                                            # crea un gruppo con il nome del virtualhost.

  adduser $user					                                              # crea un utente con il nome del virtualhost.
  usermod -g $vHost $vHost			                                        # associa l’utente [$vhost] al gruppo [$vhost] come gruppo primario. (gruppo, user)
  usermod -a -G $vHost apache			                                      # associa l’utente apache al gruppo [$vhost] come gruppo secondario. (gruppo, user)
fi


currPath=$documentRoot/$vHost
documentRoot=$currPath/public
libsRoot=$currPath/libs

echo =================================================================
echo - Hcore Set Permissions: $vHost
echo -----------------------------------------------------------------
if [[ "$myUser" == "root" ]]; then
    groupadd developers
    adduser $vHost					                                              # crea un utente con il nome del virtualhost.
    groupadd $vHost 					                                            # crea un gruppo con il nome del virtualhost.
    usermod -g $vHost $vHost			                                        # associa l’utente [$vhost] al gruppo [$vhost] come gruppo primario. (gruppo, user)
    usermod -a -G $vHost apache			                                    # associa l’utente apache al gruppo [$vhost] come gruppo secondario. (gruppo, user)

    #---------------------------------------
    echo chown $vHost:$vHost $currPath

    chown -R $vHost $currPath		                                        # reimposta l’owner di tutte le folder /data/www/[$vhost]
    chgrp -R $vHost $currPath		                                        # reimposta il group di tutte le folder /data/www/[$vhost]

    #---------------------------------------
    echo chmod 754 g+s {} + $currPath
    echo setfacl -m d:u::rwx {} + $currPath
    echo ssetfacl -m d:o::r {} + $currPath

    find $currPath -type d -exec chmod g+s {} +                               #This will make all newly created files inherit the parent directory's group, instead of the user's. for folder in /data/www/[$vhost]
    find $currPath -type d -exec setfacl -m d:u::rwx {} +                     #To set the default User permissions [$vhost] in 7. for folder in /data/www/[$vhost]
    find $currPath -type d -exec setfacl -m d:o::r {} +                       #To set the default Other permissions in 0. for folder in /data/www/[$vhost]
    chmod 754 -R $currPath                                                    #reimposta i permessi in 750 di tutte le folder /data/www/[$vhost]

    #---------------------------------------
    echo chmod 774 $documentRoot
    echo setfacl -m d:g::rwx {} + $documentRoot

    find $documentRoot -type d -exec setfacl -m d:g::rwx {} +                 #To set the default Group permissions [$vhost] in 7. for folder in /data/www/[$vhost]
    chmod 774 -R $documentRoot                                                #reimposta i permessi in 774 di tutte le folder /data/www/[$vhost]/public

    echo chmod 774 $libsRoot
    echo setfacl -m d:g::rwx {} + $libsRoot

    find $libsRoot -type d -exec setfacl -m d:g::rwx {} +                     #To set the default Group permissions [$vhost] in 7. for folder in /data/www/[$vhost]
    chmod 774 -R $libsRoot                                                    #reimposta i permessi in 774 di tutte le folder /data/www/[$vhost]/libs
else
    echo Permission Denied to set Folder Permission. Change Permission Manually.
fi