#!/bin/bash
#################################################################
#  @package Hcore
#  @subpackage Deploy
#  @author Alessandro Stucchi <wolfgan@gmail.com>
#################################################################

version="2.2"
logo()
{
#http://patorjk.com/software/taag/#p=display&f=Slant&t=Hcore%20Server
#font: slant
    clear
    cat << "EOF"

    __  __                        ____             __
   / / / /________  ________     / __ \___  ____  / /___  __  __
  / /_/ / ___/ __ \/ ___/ _ \   / / / / _ \/ __ \/ / __ \/ / / /
 / __  / /__/ /_/ / /  /  __/  / /_/ /  __/ /_/ / / /_/ / /_/ /
/_/ /_/\___/\____/_/   \___/  /_____/\___/ .___/_/\____/\__, /
                                        /_/            /____/
                                                            v2.2
EOF
}

#Default Configurations
scriptPath=$PWD
documentRoot='/data/www'
databaseRoot='/data/database'
logRoot='/data/log/www'

myUser=$(whoami)
vHost=$1

logo

if [[ -z "$vHost" ]]
  then
    read -p "Domain Name (es. test.hcore.app): " vHost
fi

if [[ -n "$vHost" ]]; then
  domainPath=$documentRoot/$vHost

  echo =================================================================
  echo - Deploy
  echo -----------------------------------------------------------------
  echo Target Document Root: $domainPath
  echo

  while true
  do
   read -r -p "Do you Want Flush Cache? [Y/n] " input

   case $input in
       [yY][eE][sS]|[yY])
   flushCache=true
   break
   ;;
       [nN][oO]|[nN])
   flushCache=false
   break
          ;;
       *)
   echo "Invalid input..."
   ;;
   esac
  done

  echo =================================================================
  echo - Composer Update
  echo -----------------------------------------------------------------
  export COMPOSER_ALLOW_SUPERUSER=1

  cd $domainPath/public
  composer update

  if [[ -d $domainPath/public/app/.git ]]; then
    echo
    echo =================================================================
    echo - Update Repository: git pull
    echo -----------------------------------------------------------------
    cd $domainPath/public/app

    git reset --hard

    git pull
  fi

  source $scriptPath/hcore-vhost-permission.sh $vHost

  if [[ $flushCache == "true" ]]; then
      echo
      echo =================================================================
      echo - Fush Cache
      echo -----------------------------------------------------------------
      redis-cli flushall
  fi
fi