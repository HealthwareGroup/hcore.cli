<?php

class Colors
{
    const reset = "\x1b[0m";

    //text color
    const black = "\x1b[30m";
    const red = "\x1b[31m";
    const green = "\x1b[32m";
    const yellow = "\x1b[33m";
    const blue = "\x1b[34m";
    const magenta = "\x1b[35m";
    const cyan = "\x1b[36m";
    const white = "\x1b[37m";

    //background color
    const blackBg = "\x1b[40m";
    const redBg = "\x1b[41m";
    const greenBg = "\x1b[42m";
    const yellowBg = "\x1b[43m";
    const blueBg = "\x1b[44m";
    const magentaBg = "\x1b[45m";
    const cyanBg = "\x1b[46m";
    const whiteBg = "\x1b[47m";
}