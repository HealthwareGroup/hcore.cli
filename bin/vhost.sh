#!/bin/bash

# Variabili di configurazione
appName=""
appSecret=""
issuer=""
signature=""
configApis=""
auth=""
modules=""
version=""

# Funzione per mostrare l'help
function print_help {
  echo "Comandi disponibili per 'vhost':"
  echo "--config-appname=<nome_app>           Specifica il nome dell'applicazione"
  echo "--config-appSecret=<app_secret>       Specifica il segreto dell'applicazione"
  echo "--config-issuer=<issuer>              Specifica l'emettitore"
  echo "--config-signature=<signature>        Specifica la firma"
  echo "--config-apis={module: domainName, [...]}  Specifica le API"
  echo "--auth=<customerKey:customerSecret>   Specifica le credenziali di autenticazione"
  echo "--modules[=auth,pjbuilder]            Specifica i moduli"
  echo "--version=<versionTag>                Specifica la versione"
}

# Analizza gli argomenti
while [ "$1" != "" ]; do
  case $1 in
    --config-appname )    shift
                          appName=$1
                          ;;
    --config-appSecret )  shift
                          appSecret=$1
                          ;;
    --config-issuer )     shift
                          issuer=$1
                          ;;
    --config-signature )  shift
                          signature=$1
                          ;;
    --config-apis )       shift
                          configApis=$1
                          ;;
    --auth )              shift
                          auth=$1
                          ;;
    --modules )           shift
                          modules=$1
                          ;;
    --version )           shift
                          version=$1
                          ;;
    --help )              print_help
                          exit
                          ;;
    * )                   echo "Errore: Opzione non valida '$1'."
                          print_help
                          exit 1
  esac
  shift
done

# Utilizza le variabili configurate per eseguire le operazioni necessarie
echo "Nome applicazione: $appName"
echo "Segreto applicazione: $appSecret"
echo "Emettitore: $issuer"
echo "Firma: $signature"
echo "Configurazione API: $configApis"
echo "Credenziali autenticazione: $auth"
echo "Moduli: $modules"
echo "Versione: $version"
