#!/bin/bash

# Definizione dei codici di colore
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
RED='\033[0;31m'
NC='\033[0m' # No Color

# THE DEFAULTS INITIALIZATION - OPTIONALS
_script_dir="$(dirname "$(dirname "$(readlink -f "$0")")")"

# Variabili di configurazione
declare -A dbConfigs

dbMigrate="preview"
dbSeed=""
domain=""

# Funzione per mostrare l'help
function print_help {
  echo "Comandi disponibili per 'db':"
  echo "--config=mysql://[dbUser]:[dbSecret]@[dbHost]/[dbName]      Specifica la configurazione del database MySQL"
  echo "--config=mongodb://[dbUser]:[dbSecret]@[dbHost]/[dbName]    Specifica la configurazione del database MongoDB"
  echo "--migrate[=safe|all|preview]                                Esegue la migrazione del database"
  echo "--seed[=clients,users,phr_contents]                         Esegue l'inserimento di dati di esempio nel database"
}
function print_error {
   echo -e "${RED}${1}${NC}"
   echo
}

# Funzione per il controllo formale della configurazione del database
function check_db_config {
  local dbConfig="$1"

  # Controllo formale della configurazione del database
  if [[ $dbConfig =~ ^(.*?):\/\/([^:]+):([^@]+)@([^/]+)\/([^/]+)$ ]]; then
      echo "${BASH_REMATCH[1]}"
    else
      echo "Errore: Configurazione del database non valida: $dbConfig"
      print_help
      exit 1
  fi
}
# Funzione per verificare l'esistenza del file config.php
function verify_config_file {
  local configFilePath="/data/www/$1/public/config.php"

  if [[ -f "$configFilePath" ]]; then
    echo "$configFilePath"
  else
    echo "Errore: Il file $configFilePath non esiste."
    exit 1
  fi
}

# Funzione per popolare le variabili nel file template config.php per il database specifico
function populate_db_config_variables {
  local dbConfig="$1"

  local dbType=""
  local dbUser=""
  local dbSecret=""
  local dbHost=""
  local dbName=""

  configFile=$(verify_config_file "$domain")

  # Controllo formale della configurazione del database
  if [[ $dbConfig =~ ^(.*?):\/\/([^:]+):([^@]+)@([^/]+)\/([^/]+)$ ]]; then
      dbType="${BASH_REMATCH[1]}"
      dbUser="${BASH_REMATCH[2]}"
      dbSecret="${BASH_REMATCH[3]}"
      dbHost="${BASH_REMATCH[4]}"
      dbName="${BASH_REMATCH[5]}"

      echo "Configurazione del database $dbType valida:"
      echo " - User: $dbUser"
      echo " - Secret: $dbSecret"
      echo " - Host: $dbHost"
      echo " - Database: $dbName"

      # Popolare le variabili nel file template config.php per il database specifico
        sed -i "s|'@${dbType}host'|'$dbHost'|g" \
            -e "s|'@${dbType}name'|'$dbName'|g" \
            -e "s|'@${dbType}user'|'$dbUser'|g" \
            -e "s|'@${dbType}password'|'$dbSecret'|g" \
            "$configFile"

      echo "File di configurazione $configFile popolato correttamente."
  fi
}

# Loop principale per leggere gli argomenti
while [[ $1 != "" ]]; do
  case $1 in
    --config=*)
      dbConfigs["$(check_db_config "${1#--config=}")"]="${1#--config=}"
      ;;
    --migrate=*)
      dbMigrate="${1#--migrate=}"
      ;;
    --migrate)
      dbMigrate="preview"
      ;;
    --seed*)
      dbSeed="${1#--seed=}"
      ;;
    --help )
      print_help
      exit
      ;;
    *)
      if [[ -d "/data/www/$1/public" ]]; then
        domain="$1"
      else
        print_error "Host non trovato: /data/www/$1/public"
      fi
      ;;
  esac
  shift
done

if [[ -z $domain ]]; then
  print_help
  exit 1
fi

for dbConfigValue in "${dbConfigs[@]}"; do
  populate_db_config_variables "$dbConfigValue"
done

if [[ -n "$dbMigrate" ]]; then
  php "$_script_dir/scripts/dbMigrate.php" "$dbMigrate" "$domain"
fi

if [[ -n "$dbSeed" ]]; then
  php "$_script_dir/scripts/dbSeed.php" "$dbSeed" "$domain"
fi
