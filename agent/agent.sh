#!/bin/bash

version="1.0"

logo()
{
#http://patorjk.com/software/taag/#p=display&f=Slant&t=Hcore%20Agent
#font: slant
    clear
    cat << "EOF"

    __  __                        ___                    __
   / / / /________  ________     /   | ____ ____  ____  / /_
  / /_/ / ___/ __ \/ ___/ _ \   / /| |/ __ `/ _ \/ __ \/ __/
 / __  / /__/ /_/ / /  /  __/  / ___ / /_/ /  __/ / / / /_
/_/ /_/\___/\____/_/   \___/  /_/  |_\__, /\___/_/ /_/\__/
                                    /____/
                                                            v$version
EOF
}

die()
{
	local _ret=$2
	test -n "$_ret" || _ret=1
  print_help >&2
	echo "$1" >&2
}

# THE DEFAULTS INITIALIZATION - OPTIONALS
_tmp_dir="/tmp/hagent"
_script_dir="$(dirname "$(readlink -f "$0")")"
_arg_domain=""
_arg_sender=""
_arg_sender_to=""
_arg_sender_auth=""

print_help()
{
    logo
    echo "Usage: $(basename "$0") <DOMAIN_NAME> [-h] [-v] [-s] [--sender=<whatsapp,telegram,email>] [--sender-to=<phone,email>] [--sender-auth=<token,url>]
    sender:
        --sender        Enable Send notification <whatsapp,telegram,email> (default: none)
        --sender-to     Send notification to <phone,email> (default: none)
        --sender-auth   Send notification with <token,url> (default: none)
    helper:
        -h              Prints help
        -v              Prints version" $version;
}

parse_commandline()
{
	while test $# -gt 0
	do
		_key="$1"
		case "$_key" in
		  --sender=*)
		    if [[ -f "$_script_dir/sender/${_key##--sender=}.sh" ]]; then
          _arg_sender="${_key##--sender=}"
        else
          die "${_key##--sender=} sender does not exist " 1
          return 1
        fi
        ;;
		  --sender-to=*)
        _arg_sender_to="${_key##--sender-to=}"
        ;;
      --sender-auth=*)
        _arg_sender_auth="${_key##--sender-auth=}"
        ;;
			-v|--version)
				echo Agent v$version
				return 1
				;;
			-v*)
				echo Agent v$version
				return 1
				;;
			-h|--help)
				print_help
				return 1
				;;
			-h*)
				print_help
				return 1
				;;
			*)
        _arg_domain=$_key
				;;
		esac
		shift
	done

  if [ -z "$_arg_domain" ]; then
    die "Domain is required" 1
  fi

	return 0
}

run()
{
  if [ ! -d $_tmp_dir ]; then
      mkdir $_tmp_dir
  fi

  for probeScript in $_script_dir/probes/*.sh; do
    probeMessage+=$(bash $probeScript $_arg_domain)

    if [ -n "$probeMessage" ]; then
      echo $probeMessage
      if [ -n "$_arg_sender" ]; then
        $_script_dir/sender/$_arg_sender.sh "$probeMessage" --to=$_arg_sender_to --auth=$_arg_sender_auth
      fi
    fi
    probeMessage=""
  done


}


if parse_commandline "$@"; then
  run
fi