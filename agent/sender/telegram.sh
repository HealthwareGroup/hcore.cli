#!/bin/bash

version=1

die()
{
	local _ret=$2
	test -n "$_ret" || _ret=1
  print_help >&2
	echo "$1" >&2
}

begins_with_short_option()
{
	local first_option all_short_options='vh'
	first_option="${1:0:1}"
	test "$all_short_options" = "${all_short_options/$first_option/}" && return 1 || return 0
}

# THE DEFAULTS INITIALIZATION - OPTIONALS
_arg_message=""
_arg_sender_to=""
_arg_sender_auth=""
_telegram_api='https://api.telegram.org'

print_help()
{
    echo "Usage: $(basename "$0") [-h] [-v] [-s] <MESSAGE> [--to=<CHANNEL_ID>] [--auth=<TOKEN>]
    helper:
        -h              Prints help
        -v              Prints version" $version;
}

print_status()
{
  return 0
}

parse_commandline()
{
  if [ $# -eq 0 ]
    then
      die "arg required" 1
      return 1
  fi
	while test $# -gt 0
	do
		_key="$1"
		case "$_key" in
      --to=*)
       _arg_sender_to="${_key##--to=}"
       ;;
      --auth=*)
       _arg_sender_auth="${_key##--auth=}"
       ;;
			-v|--version)
				echo errorlog monitor v$version
				return 1
				;;
			-v*)
				echo errorlog monitor v$version
				return 1
				;;
			-h|--help)
				print_help
				return 1
				;;
			-h*)
				print_help
				return 1
				;;
		  -s|--status)
        print_status
        return 1
        ;;
      -s*)
        print_status
        return 1
        ;;
			*)
        _arg_message="${_key}"
        ;;
		esac
		shift
	done

  if [ -z "$_arg_message" ]; then
    die "message is required" 1
  fi
  if [ -z "$_arg_sender_auth" ]; then
    die "--auth is required" 1
  fi
  if [ -z "$_arg_sender_to" ]; then
    die "--to is required" 1
  fi
	return 0
}

run()
{
  echo "$_telegram_api/$_arg_sender_auth/sendMessage?chat_id=$_arg_sender_to&text='$_arg_message'&parse_mode=markdown"
  curl --get \
    --data-urlencode  "chat_id=$_arg_sender_to" \
    --data-urlencode  "text=$_arg_message" \
    --data-urlencode "parse_mode=markdown" \
    $_telegram_api/$_arg_sender_auth/sendMessage
}


if parse_commandline "$@"; then
  run
fi