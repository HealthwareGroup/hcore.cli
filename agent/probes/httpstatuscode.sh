#!/bin/bash

version=1.0
severity=low

die()
{
	local _ret=$2
	test -n "$_ret" || _ret=1
  print_help >&2
	echo "$1" >&2
}

# THE DEFAULTS INITIALIZATION - OPTIONALS
_script_name=$(basename "$0")
_script_dir="$(dirname "$(readlink -f "$0")")"
_tmp_dir="/tmp/hagent"
_arg_domain=""
_store="$_tmp_dir/${_script_name%.*}"
_store_temp="$_tmp_dir/temp_${_script_name%.*}"
_store_diff="$_tmp_dir/diff_${_script_name%.*}"

env=$(cat $_script_dir/../conf/"${_script_name%%.sh}".ini) api_filter=($(echo "$env" | tr " " "\n"))

print_help()
{
    echo "Usage: $(basename "$0") <DOMAIN_NAME> [-h] [-v] [-s]
    helper:
        -h              Prints help
        -v              Prints version" $version;
}

parse_commandline()
{
  if [ $# -eq 0 ]
    then
      die "arg required" 1
      return 1
  fi
	while test $# -gt 0
	do
		_key="$1"
		case "$_key" in
			-v|--version)
				echo Probe: errorlog v$version
				return 1
				;;
			-v*)
				echo Probe: errorlog v$version
				return 1
				;;
			-h|--help)
				print_help
				return 1
				;;
			-h*)
				print_help
				return 1
				;;
			*)
        _arg_domain=$_key
				;;
		esac
		shift
	done

  if [ -z "$_arg_domain" ]; then
    die "Domain is required" 1
  fi

  if [[ -f "/data/log/www/$_arg_domain-ssl-requests.log" ]]; then
    _requestlog_path="/data/log/www/$_arg_domain-ssl-requests.log"
  else
    die "$_arg_domain-ssl-requests.log file not found " 1
  fi

  _store="$_store-$_arg_domain.log"

	return 0
}

run()
{
  #legge il log, lo filtra e lo scrive in store_temp
  cat "$_requestlog_path" |
      awk '{ $1 = $2 = $3 = $4 = $5 = $6 = $8 = $10 = "" ; print }' |
      sort |
      uniq -c |
      awk '
  {
      printf "%-20s", $2 != p ? $2 : "", $1 ;
      for (i=3 ; i<=NF ; i++) printf ":%s %d",$i, $1 ;
      printf "\n"
  }' > "$_store_temp"

  if [ ! -f "$_store" ]; then
    cp "$_store_temp" "$_store"
  fi
  #fa la differenza tra il log in store_temp e il log precedente e lo scrive in un file diff
  diff "${_store_temp}" "${_store}" | sed 's/^< /+ /g' | grep " " | sed 's/^> /- /g' > "${_store_diff}"
  #cicla il file diff e effettua i calcoli per i totali
  readarray -t lines < "$_store_diff"
  declare -A to_return
  for line in "${lines[@]}"; do
     arr_line=($line)
     op=${arr_line[0]}
     api=${arr_line[1]}
     value=${arr_line[2]}
     temp=${api//\//_}
     index=${temp//:/_}
     if [[ $op = "-" ]]; then
        arr_line_prev=(${to_return[$index]})
        value_prev=${arr_line_prev[1]}
        value_diff=$((value_prev - value))
        to_return[$index]="$api $value_diff"
      else
        to_return[$index]+="$api $value"
    fi
    #se c'è un filtro sulle api elimina le api non richieste
    for i in "${api_filter[@]}"; do
      unset=1
      if [[ "$i" == "$api" ]]; then
        unset=0
        break
      fi
    done
    if [[ "$unset" = 1 ]]; then
     unset to_return[$index]
    fi
  done
  printf -v print '%s\n' "${to_return[@]}"
  printf "*Host*: $_arg_domain \\n*Severity*: $severity \\n*Description*: ${print} \\n*Date*: $(date)"

  cp "$_store_temp" "$_store"
  rm "$_store_temp"
  rm "$_store_diff"
}

if parse_commandline "$@"; then
  run
fi