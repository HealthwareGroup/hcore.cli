#!/bin/bash

version=1.0
severity=high

die()
{
	local _ret=$2
	test -n "$_ret" || _ret=1
  print_help >&2
	echo "$1" >&2
}

# THE DEFAULTS INITIALIZATION - OPTIONALS
_script_name=$(basename "$0")
_script_dir="$(dirname "$(readlink -f "$0")")"
_tmp_dir="/tmp/hagent"
_arg_domain=""
_store="$_tmp_dir/${_script_name%.*}"
_errorlog_path=""

print_help()
{
    echo "Usage: $(basename "$0") <DOMAIN_NAME> [-h] [-v] [-s]
    helper:
        -h              Prints help
        -v              Prints version" $version;
}

parse_commandline()
{
  if [ $# -eq 0 ]
    then
      die "arg required" 1
      return 1
  fi
	while test $# -gt 0
	do
		_key="$1"
		case "$_key" in
			-v|--version)
				echo Probe: errorlog v$version
				return 1
				;;
			-v*)
				echo Probe: errorlog v$version
				return 1
				;;
			-h|--help)
				print_help
				return 1
				;;
			-h*)
				print_help
				return 1
				;;
			*)
        _arg_domain=$_key
				;;
		esac
		shift
	done

  if [ -z "$_arg_domain" ]; then
    die "Domain is required" 1
  fi

  if [[ -f "/data/log/www/$_arg_domain-ssl-error.log" ]]; then
    _errorlog_path="/data/log/www/$_arg_domain-ssl-error.log"
  else
    die "$_arg_domain-ssl-error.log file not found " 1
  fi

  _store="$_store-$_arg_domain.log"

	return 0
}

run()
{
  size=$(wc -c "$_errorlog_path" | awk '{print $1}')
  if [ ! -f $_store ]; then
      size_last=size
  else
      size_last=$(cat "$_store")
  fi

  if ((size > size_last)); then
      printf "*Host*: $_arg_domain \\n*Severity*: $severity \\n*Description*: Error found in $_errorlog_path \\n*Date*: $(date)"
  fi
  echo "$size" > "$_store"
}

if parse_commandline "$@"; then
  run
fi