#!/bin/bash

version=1.0.0

die()
{
	local _ret=$2
	test -n "$_ret" || _ret=1
  print_help >&2
	echo "$1" >&2
}

# THE DEFAULTS INITIALIZATION - OPTIONALS
_script_dir="$(dirname "$(readlink -f "$0")")"
_arg_cron="* * * * *"
_arg_domain=""

print_help()
{
    echo "Usage: $(basename "$0") <DOMAIN_NAME> [--cron=<CRONTAB>]
    helper:
        -h              Prints help
        -v              Prints version" $version;
}

parse_commandline()
{
  if [ $# -eq 0 ]
    then
      die "arg required" 1
      return 1
  fi

  while test $# -gt 0
  	do
  		_key="$1"
  		case "$_key" in
  		  --cron=*)
          _arg_cron="${_key##--cron=}"
          ;;
  			-v|--version)
  				echo Agent Cron v$version
  				return 1
  				;;
  			-v*)
  				echo Agent Cron v$version
  				return 1
  				;;
  			-h|--help)
  				print_help
  				return 1
  				;;
  			-h*)
  				print_help
  				return 1
  				;;
  			*)
  				_arg_domain=$_key
  				;;
  		esac
  		shift
  	done

    if [ -z "$_arg_domain" ]; then
      die "Domain is required" 1
    fi

  	return 0
}

run()
{
  crontab -l > mycron
  echo "$_arg_cron root      $_script_dir/agent.sh $_arg_domain --sender=telegram --sender-to=-1001821349031 --sender-auth=bot6205237554:AAEOssPu2E_c_BDK3v-VMaEdU706RUhKP7M" >> mycron
  crontab mycron
  rm mycron

  return 1
}

if parse_commandline "$@"; then
  run
fi