#!/bin/bash

# hcore server
# hcore db [domain] --config=mysql://[dbUser]:[dbSecret]@[dbHost]/[dbName]
#                   --config=mongodb://[dbUser]:[dbSecret]@[dbHost]/[dbName]
#                   --migrate[=safe] or --migrate=all or --migrate=preview
#                   --seed or --seed=clients,users,phr_contents
# hcore vhost [domain]
#                   --config-appname=[appName]
#                   --config-appSecret=[appSecret]
#                   --config-issuer=[issuer]
#                   --config-signature=[signature]
#                   --config-[...]=[...]
#                   --config-apis={module: domainName, [...]}
#                   --auth=[customerKey:customerSecret]
#                   --modules or --modules=auth,pjbuilder                 //se non impostato carica solo il core
#                   --version=[versionTag]
# hcore deploy [domain]
# hcore clearcache [domain]
# hcore dumpautoload [domain]
# hcore repair [domain]
# hcore user [domain] create [username] [secret]
# hcore user [domain] delete [username]
# hcore agent [domain] install [cronReg]
# hcore agent [domain] remove
# hcore module [domain] create
# hcore module [domain] remove [moduleName]
# hcore module [domain] add [moduleName]
# hcore cors [domain] add
# hcore cors [domain] remove
# hcore certs renew
# hcore certs [domain] create [cronReg]
# hcore log [domain]
# hcore update
# hcore k8 tag
# hcore k8 retag
# hcore k8 pods

version="1.0"

logo()
{
#http://patorjk.com/software/taag/#p=display&f=Slant&t=Hcore%20CLI
#font: slant
    clear
    cat << "EOF"

    __  __                        ________    ____
   / / / /________  ________     / ____/ /   /  _/
  / /_/ / ___/ __ \/ ___/ _ \   / /   / /    / /
 / __  / /__/ /_/ / /  /  __/  / /___/ /____/ /
/_/ /_/\___/\____/_/   \___/   \____/_____/___/
                                                  v1.0
EOF
}

die()
{
	local _ret=$2
	test -n "$_ret" || _ret=1
  print_help >&2
	echo "$1" >&2
}


# THE DEFAULTS INITIALIZATION - OPTIONALS
_script_dir="$(dirname "$(readlink -f "$0")")"

# Definizione dei codici di colore
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
RED='\033[0;31m'
NC='\033[0m' # No Color

function print_help {
  script=$1
  if [ -x "$script" ]; then
    "$@"
  else
    echo -e "${YELLOW}Errore: Script $script non trovato.${NC}"
  fi
}

# Se è specificato un sotto-comando, mostra solo il risultato del comando corrispondente
if [ $# -gt 0 ]; then
  selected_command="$1"
  script="$_script_dir/bin/$selected_command.sh"
  echo -e "${GREEN}# $selected_command${NC}"
  shift
  print_help "$script" "$@"
  echo
else
  # Mostra tutti i comandi nella directory /bin
  for script in $_script_dir/bin/*.sh; do
    command=$(basename "$script" .sh)
    echo -e "${GREEN}# $command${NC}"
    print_help "$script"
    echo
  done
fi
